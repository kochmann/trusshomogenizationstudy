import pandas as pd
from utils import *
from tqdm import tqdm
from utils import plot_lattice, extract_conn_data

# load data
solid = pd.read_csv('/mnt/io/output/Data_Solid.csv')
lattice_vectors = pd.read_csv('/mnt/io/input/lattice_vectors.csv')

# select lattice
names = solid["name"].unique()
conn_data = np.zeros((len(names),7))

for i in tqdm(range(len(names))):
    lattice_no = int(names[i].rsplit('E', 1)[1]) - 1
    lattice = pd.read_csv('/mnt/io/input/meshes/UC_' + str(lattice_no) + '.dat', header=None)

    # preprocess coordinates & connectivities
    split_conn_coord = lattice.iloc[:,0].str.contains('connectivity')
    index_split = split_conn_coord.index[split_conn_coord]
    coord = lattice.iloc[:index_split[0]].to_numpy().astype(float)
    conn = lattice.iloc[index_split[0]+1:,:2].to_numpy().astype(int)
    # preprocess lattice vectors
    lattice_vector_pd = lattice_vectors.loc[lattice_vectors['UC_'] == lattice_no]
    lattice_vector = lattice_vector_pd.iloc[:,1:].to_numpy().astype(float)

    # extract connectivity information
    conn_avg, conn_std, conn_min, conn_max, conn_number, nodes_number = extract_conn_data(conn,coord,lattice_vector,tol=1.e-3)
    conn_data[i,:] = lattice_no, conn_avg, conn_std, conn_min, conn_max, conn_number, nodes_number

np.savetxt('/mnt/io/output/conn_eval.csv', conn_data, delimiter=',', header='UC_, conn_avg, conn_std, conn_min, conn_max, conn_no, nodes_no', comments='', fmt='%.14f')