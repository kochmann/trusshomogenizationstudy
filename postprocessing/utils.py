import numpy as np
import plotly.express as plt
import plotly.graph_objects as go
import operator
import functools


def extract_conn_data(conn, coord, lattice_vectors,tol):
    coord = transform_frac_to_eucl(coord, lattice_vectors)
    tess_conn, coord = tesselate_orthogonal_UC(conn, coord)
    red_conn, _ = remove_overlapping_nodes(tess_conn,coord,tol)
    conn_avg, conn_std, conn_min, conn_max, conn_number, nodes_number = compute_connectivity_information(conn, red_conn)
    return conn_avg, conn_std, conn_min, conn_max, conn_number, nodes_number


def transform_frac_to_eucl(coord,lattice_vectors):
    lattice_vector_matrix = lattice_vectors.reshape((3,3)).transpose()
    inverse_map = np.linalg.inv(lattice_vector_matrix)
    final_lattice_coord = np.linalg.multi_dot([coord,inverse_map.transpose()])
    return final_lattice_coord


def tesselate_orthogonal_UC(conn,coord):
    temp_conn = conn.copy()
    temp_coord = coord.copy()
    UC_nodes = len(coord)
    for i in range(3):
        temp_coord[:,i] = temp_coord[:,i]+1
        coord = np.concatenate((coord,np.stack((temp_coord[:,0],temp_coord[:,1],temp_coord[:,2]),axis=1)))
        temp_coord[:,i] = temp_coord[:,i]-2
        coord = np.concatenate((coord,np.stack((temp_coord[:,0],temp_coord[:,1],temp_coord[:,2]),axis=1)))
        conn = np.concatenate((conn,temp_conn+UC_nodes*3**i,temp_conn + 2*UC_nodes*3**i))
        temp_conn = conn.copy()
        temp_coord = coord.copy()
    return conn, coord


def remove_overlapping_nodes(conn,coord,tol):
        tot_nodes = len(coord)
        red_conn = conn
        duplicate_nodes = []
        for i in range(tot_nodes):
            for j in range((i+1), tot_nodes):
                if np.linalg.norm(coord[i,:] - coord[j,:]) < tol:
                    red_conn[red_conn==j] = i
                    duplicate_nodes.append(j)

        if duplicate_nodes:
            duplicate_nodes = np.unique(duplicate_nodes)
            duplicate_nodes = np.sort(duplicate_nodes)[::-1]
            for i in duplicate_nodes:
                red_conn[red_conn>i] = red_conn[red_conn>i] - 1
        
        # delete duplicate nodes and preserve order
        _, idx = np.unique(coord, axis=0, return_index=True)
        red_coord = coord[np.sort(idx)]

        # sort connectivities in ascending direction
        for i in range(len(red_conn)):
            if red_conn[i,0]>red_conn[i,1]:
                temp = red_conn[i,1]
                red_conn[i,1] = red_conn[i,0]
                red_conn[i,0] = temp

        # delete duplicate connectivities (similar rows)
        red_conn = np.unique(red_conn,axis=0)

        return red_conn, red_coord


def compute_connectivity_information(orig_conn, tesselated_conn):
    nodes_number = np.max(orig_conn) + 1
    full_conn_number = len(tesselated_conn)
    connectivities_per_node = np.zeros(nodes_number)
    for i in range(nodes_number):
        conn_count = 0
        for j in range(full_conn_number):
            if i == tesselated_conn[j,0] or i == tesselated_conn[j,1]:
                conn_count = conn_count + 1
            # do not consider nodes with 2 or less connectivities
            if conn_count > 2:
                connectivities_per_node[i] = conn_count
    # remove all zero entries
    connectivities_per_node = [i for i in connectivities_per_node if i != 0]
    red_nodes_number = len(connectivities_per_node)
    # compute sought statistical quantities
    conn_avg = np.mean(connectivities_per_node)
    conn_std = np.std(connectivities_per_node)
    conn_min = int(np.min(connectivities_per_node))
    conn_max = int(np.max(connectivities_per_node))
    conn_number = int(np.sum(connectivities_per_node))
    return conn_avg, conn_std, conn_min, conn_max, conn_number, red_nodes_number


def plot_lattice(coordinates,connectivity):
    fig = plt.scatter_3d(coordinates, x=0, y=1, z=2, width=100)
    fig = connect_points(fig,coordinates,connectivity)
    fig.update_layout(title_text='Lattice', title_x=0.5)
    fig.update_traces(marker=dict(size=5,
                            line=dict(width=20)))
    fig.show()


def connect_points(fig,coordinates,connectivity):
    fig_temp = [fig]
    for i in range(len(connectivity)):
        x1, x2 = coordinates[connectivity[i,0],0], coordinates[connectivity[i,1],0]
        y1, y2 = coordinates[connectivity[i,0],1], coordinates[connectivity[i,1],1]
        z1, z2 = coordinates[connectivity[i,0],2], coordinates[connectivity[i,1],2]
        fig_temp.append(plt.line_3d(x=[x1,x2],y=[y1,y2],z=[z1,z2]))
    fig_temp = go.Figure(data=functools.reduce(operator.add, [_.data for _ in fig_temp]))
    return fig_temp
