#include "fileReader/read_config.h"
#include "fileReader/writer.h"
#include "mesh/assign_lattice_to.h"
#include "mesh/build_periodic_point_cloud.h"
#include "mesh/construct_paralleliped_cell.h"
#include "mesh/construct_voronoi_cell.h"
#include "mesh/gmsh/convert_segment_mesh_to_periodic_volume_mesh.h"
#include "mesh/read_mesh_from_file.h"

#include "ae108/cpppetsc/Context.h"
#include "ae108/cpppetsc/ParallelComputePolicy.h"
#include "ae108/elements/CoreElement.h"
#include "ae108/elements/embedding/IsoparametricEmbedding.h"
#include "ae108/elements/integrator/IsoparametricIntegrator.h"
#include "ae108/elements/materialmodels/Hookean.h"
#include "ae108/elements/materialmodels/compute_tangent_matrix.h"
#include "ae108/elements/quadrature/Quadrature.h"
#include "ae108/elements/shape/Tet4.h"
#include "ae108/elements/tensor/as_matrix_of_rows.h"
#include "ae108/elements/tensor/as_two_tensor.h"
#include "ae108/elements/tensor/as_vector.h"
#include "ae108/homogenization/PBCFunctor.h"
#include "ae108/homogenization/RVEBase.h"
#include "ae108/homogenization/materialmodels/Condensation.h"
#include <Eigen/Dense>
#include <ae108/cmdline/CommandLineOptionParser.h>
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/iota.hpp>

#include <iostream>

using namespace ae108;

using Policy = cpppetsc::ParallelComputePolicy;
using Mesh = cpppetsc::Mesh<Policy>;
using Context = cpppetsc::Context<Policy>;

namespace embedding = ae108::elements::embedding;
namespace integrator = ae108::elements::integrator;
namespace shape = ae108::elements::shape;
namespace quadrature = ae108::elements::quadrature;
namespace tensor = ae108::elements::tensor;
namespace materialmodels = ae108::elements::materialmodels;

constexpr auto dimension = Mesh::size_type{3};
constexpr auto dof_per_vertex = Mesh::size_type{3};
constexpr auto QuadratureOrder = Mesh::size_type{1};

using MaterialModel = materialmodels::Hookean<dimension>;
using Shape = shape::Tet4;
using Embedding = embedding::IsoparametricEmbedding<Shape>;
using Quadrature =
    quadrature::Quadrature<quadrature::QuadratureType::Simplex, dimension, 1>;
using Integrator = integrator::IsoparametricIntegrator<Shape, Quadrature>;
using Element = elements::CoreElement<MaterialModel, Integrator>;
using RVE = homogenization::RVEBase<Policy, Element, dimension>;

using Condensation = homogenization::materialmodels::Condensation<RVE>;

using Point = std::array<double, dimension>;

// Creates a periodic mesh for a unit cell of a periodic truss lattice from
// nodal positions and connectivity information
// of the unit cell contained in the .dat file

// After creating the mesh the effective tangent matrix is calculated by
// perturbing the deformation gradient

int main(int argc, char **argv) {

  const auto context = Context(&argc, &argv);

  std::string input_folder = "/mnt/io/data";
  double density = 0.1;
  std::size_t row = 0;
  std::string output_folder = "/mnt/io/";

  cmdline::CommandLineOptionParser(std::cerr)
      .withOption("density,d", "Specify the radius of the struts.", &density)
      .withOption("id,c", "Specify the row in the dataset.", &row)
      .withOption("input_folder,i", "Specify the location of the dataset.",
                  &input_folder)
      .withOption("output_folder,o", "Specify the location of the output.",
                  &output_folder)
      .parse(argc, argv);

  std::string data_input = input_folder + "/data.csv";

  std::ifstream dataset(data_input);

  const auto config = read_config<dimension>(dataset, row);

  const auto lattice_vectors = config.lattice_vectors;

  constexpr int elementType = 4;
  constexpr int order = 1;

  for (const auto &vector : lattice_vectors)
    std::cout << tensor::as_vector(&vector) << std::endl;

  std::string name = config.name;

  std::string dat_file = "UC_" + config.number;

  const auto segment_mesh = mesh::read_mesh_from_file<Point>(
      input_folder + "/unitcells/" + dat_file + ".dat");

  std::vector<Point> vertex_positions(segment_mesh.number_of_positions());
  std::vector<int> indices(segment_mesh.number_of_positions());
  std::iota(indices.begin(), indices.end(), 0);
  for (const auto index : indices)
    vertex_positions[index] = segment_mesh.position_of_vertex(index);

  const auto origin = [&]() {
    Point origin = {0., 0., 0.};
    for (const auto node : vertex_positions)
      tensor::as_vector(&origin) += tensor::as_vector(&node);

    tensor::as_vector(&origin) /= segment_mesh.number_of_positions();

    return origin;
  }();

  const double volume =
      elements::tensor::as_matrix_of_rows(&lattice_vectors).determinant();

  const auto radius = [&]() {
    const auto beam_mesh = RVE::mesh_type::fromConnectivity(
        dimension, segment_mesh.connectivity(),
        segment_mesh.number_of_positions(), Element::degrees_of_freedom());
    const auto element_source = mesh::assign_lattice_to(
        segment_mesh.connectivity(), vertex_positions, lattice_vectors);

    double weight = 0.;
    for (const auto &element : beam_mesh.localElements()) {
      std::array<typename Mesh::value_type, dimension> element_axis;
      elements::tensor::as_vector(&element_axis) =
          elements::tensor::as_vector(&segment_mesh.position_of_vertex(
              segment_mesh.connectivity().at(element.index())[1])) -
          elements::tensor::as_vector(&segment_mesh.position_of_vertex(
              segment_mesh.connectivity().at(element.index())[0]));

      weight += 1. /
                std::count(element_source.begin(), element_source.end(),
                           element_source[element.index()]) *
                elements::tensor::as_vector(&element_axis).norm();
    }

    double radius = sqrt((density * volume) / (M_PI * weight));
    return radius;
  }();

  const double granularity = radius / 5;

  if (density > 0.101)
    const double granularity = radius / 5;
  else if (density > 0.501 && density < 0.101)
    const double granularity = radius / 4;
  else
    const double granularity = radius / 3.5;

  std::cout << name << std::endl;
  std::cout << "Radius " << radius << std::endl;
  std::cout << "Granularity " << granularity << std::endl;

  std::vector<double> radii(segment_mesh.connectivity().size(), radius);

  const auto domain = [](const std::array<Point, dimension> lattice_vectors,
                         const Point &origin) {
    return mesh::construct_paralleliped_cell(lattice_vectors, origin);
  }(lattice_vectors, origin);

  double mass = 0;

  const auto [geometry, source_map] =
      mesh::convert_segment_mesh_to_periodic_volume_mesh(
          segment_mesh, radii, domain, granularity, lattice_vectors, mass);

  std::cout << "Mesh complete" << std::endl;

  double actual_density = mass / volume;

  auto mesh = RVE::mesh_type::fromConnectivity(
      dimension, geometry.connectivity(), geometry.number_of_positions(),
      Element::degrees_of_freedom());
  std::cout << mesh.totalNumberOfVertices() << std::endl;
  auto assembler = RVE::assembler_type();

  for (const auto &element : mesh.localElements()) {
    Embedding::Collection<Embedding::PhysicalPoint> points;
    std::transform(
        geometry.connectivity().at(element.index()).begin(),
        geometry.connectivity().at(element.index()).end(), points.begin(),
        [geometry = geometry](typename RVE::mesh_type::size_type nodeIndex)
            -> Embedding::PhysicalPoint {
          return geometry.position_of_vertex(nodeIndex);
        });
    assembler.emplaceElement(element, MaterialModel(1., 0.3),
                             Integrator(Embedding(points)));
  }

  RVE rve{std::move(geometry), std::move(mesh), std::move(assembler), volume};

  auto bcFunctor = homogenization::PBCFunctor<Policy, RVE::point_type>(
      rve.geometry, source_map);

  const Condensation model(std::move(rve), bcFunctor);

  const auto displacement_gradient = Condensation::DisplacementGradient{
      {{0., 0., 0.}, {0., 0., 0.}, {0., 0., 0.}}};

  const auto tangent_matrix = materialmodels::compute_tangent_matrix(
      model, 0, displacement_gradient, 0.);

  auto voigtMatrix = [&]() {
    const int voigtDim = dimension * (dimension + 1) / 2;
    Eigen::Matrix<double, voigtDim, voigtDim> voigtMatrix =
        Eigen::Matrix<double, voigtDim, voigtDim>::Zero();

    auto index = ranges::views::iota(0, dimension);
    for (auto [i, j, k, l] :
         ranges::views::cartesian_product(index, index, index, index)) {
      int dij = i == j;
      int dkl = k == l;
      int p = i * dij + (1 - dij) * (voigtDim - i - j);
      int q = k * dkl + (1 - dkl) * (voigtDim - k - l);
      voigtMatrix(p, q) = tangent_matrix[i][j][k][l];
    }
    return voigtMatrix;
  }();

  std::cout << voigtMatrix << std::endl;
  std::string outputFile = output_folder + "/" + name + ".txt";
  writeToFile(outputFile, voigtMatrix, name, granularity, density, radius,
              actual_density);
}