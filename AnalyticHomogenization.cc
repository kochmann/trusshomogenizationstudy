#include "mesh/build_periodic_point_cloud.h"
#include "mesh/construct_voronoi_cell.h"
#include "mesh/gmsh/convert_segment_mesh_to_periodic_volume_mesh.h"
#include "mesh/read_mesh_from_file.h"

#include "ae108/cpppetsc/Context.h"
#include "ae108/cpppetsc/ParallelComputePolicy.h"
#include "ae108/elements/CoreElement.h"
#include "ae108/elements/embedding/IsoparametricEmbedding.h"
#include "ae108/elements/integrator/IsoparametricIntegrator.h"
#include "ae108/elements/materialmodels/Hookean.h"
#include "ae108/elements/materialmodels/compute_tangent_matrix.h"
#include "ae108/elements/quadrature/Quadrature.h"
#include "ae108/elements/shape/Tet4.h"
#include "ae108/elements/tensor/as_matrix_of_rows.h"
#include "ae108/elements/tensor/as_two_tensor.h"
#include "ae108/elements/tensor/as_vector.h"
#include "ae108/homogenization/PBCFunctor.h"
#include "ae108/homogenization/RVEBase.h"
#include "ae108/homogenization/materialmodels/Condensation.h"
#include <Eigen/Dense>
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/iota.hpp>

#include <iostream>

using namespace ae108;

using Policy = cpppetsc::ParallelComputePolicy;
using Mesh = cpppetsc::Mesh<Policy>;
using Context = cpppetsc::Context<Policy>;

namespace embedding = ae108::elements::embedding;
namespace integrator = ae108::elements::integrator;
namespace shape = ae108::elements::shape;
namespace quadrature = ae108::elements::quadrature;
namespace tensor = ae108::elements::tensor;
namespace materialmodels = ae108::elements::materialmodels;

constexpr auto dimension = Mesh::size_type{3};
constexpr auto dof_per_vertex = Mesh::size_type{3};
constexpr auto QuadratureOrder = Mesh::size_type{1};

using MaterialModel = materialmodels::Hookean<dimension>;
using Shape = shape::Tet4;
using Embedding = embedding::IsoparametricEmbedding<Shape>;
using Quadrature =
    quadrature::Quadrature<quadrature::QuadratureType::Simplex, dimension, 1>;
using Integrator = integrator::IsoparametricIntegrator<Shape, Quadrature>;
using Element = elements::CoreElement<MaterialModel, Integrator>;
using RVE = homogenization::RVEBase<Policy, Element, dimension>;

using Condensation = homogenization::materialmodels::Condensation<RVE>;

using Point = std::array<double, dimension>;

// Creates a periodic mesh for a unit cell of a periodic truss lattice from
// nodal positions and connectivity information
// of the unit cell contained in the .dat file

// After creating the mesh the effective tangent matrix is calculated by
// perturbing the deformation gradient

int main(int argc, char **argv) {

  const auto context = Context(&argc, &argv);

  std::array<Point, dimension> lattice_vectors{
      {{1., 1., 0.}, {1., 0, 0.}, {0., 0., 1.}}};
  //    {{1., 1., 0.}, {0., 1., 1.}, {1., 0., 1.}}};
  // {{0.5, 0.5, 0.5}, {-0.5, 0.5, 0.5}, {0.5, -0.5, 0.5}}};

  constexpr double granularity = 0.1;
  constexpr int elementType = 4;
  constexpr int order = 1;
  constexpr double slenderness = 0.1;

  std::string name = "octet";

  std::cout << "Slenderness " << slenderness << std::endl;
  std::cout << "Granularity " << granularity << std::endl;
  std::cout << name << std::endl;

  const auto segment_mesh =
      mesh::read_mesh_from_file<Point>("/mnt/io/unitcells/" + name + ".dat");

  const auto origin = [&]() {
    Point origin = {0., 0., 0.};
    std::vector<int> indices(segment_mesh.number_of_positions());
    std::iota(indices.begin(), indices.end(), 0);
    for (const auto index : indices)
      tensor::as_vector(&origin) +=
          tensor::as_vector(&segment_mesh.position_of_vertex(index));

    tensor::as_vector(&origin) /= segment_mesh.number_of_positions();

    return origin;
  }();

  const double radius = [&]() {
    const auto &p0 =
        segment_mesh.position_of_vertex(segment_mesh.connectivity()[0][0]);
    const auto &p1 =
        segment_mesh.position_of_vertex(segment_mesh.connectivity()[0][1]);
    tensor::Tensor<double, dimension> axis;
    tensor::as_vector(&axis) = tensor::as_vector(&p1) - tensor::as_vector(&p0);
    double radius = tensor::as_vector(&axis).norm() * 0.5 * slenderness;
    return radius;
  }();

  std::cout << "Radius " << radius << std::endl;
  std::vector<double> radii(segment_mesh.connectivity().size(), radius);

  const auto domain = [](const std::array<Point, dimension> lattice_vectors,
                         const Point &origin) {
    const auto pointCloud = mesh::build_periodic_point_cloud(lattice_vectors);
    return mesh::construct_voronoi_cell(pointCloud, origin);
  }(lattice_vectors, origin);

  double mass = 0;

  const auto [geometry, source_map] =
      mesh::convert_segment_mesh_to_periodic_volume_mesh(
          segment_mesh, radii, domain, granularity, lattice_vectors, mass);

  std::cout << "Mesh complete" << std::endl;

  auto mesh = RVE::mesh_type::fromConnectivity(
      dimension, geometry.connectivity(), geometry.number_of_positions(),
      Element::degrees_of_freedom());
  std::cout << mesh.totalNumberOfVertices() << std::endl;
  auto assembler = RVE::assembler_type();

  for (const auto &element : mesh.localElements()) {
    Embedding::Collection<Embedding::PhysicalPoint> points;
    std::transform(
        geometry.connectivity().at(element.index()).begin(),
        geometry.connectivity().at(element.index()).end(), points.begin(),
        [geometry = geometry](typename RVE::mesh_type::size_type nodeIndex)
            -> Embedding::PhysicalPoint {
          return geometry.position_of_vertex(nodeIndex);
        });
    assembler.emplaceElement(element, MaterialModel(1., 0.3),
                             Integrator(Embedding(points)));
  }

  const double volume =
      elements::tensor::as_matrix_of_rows(&lattice_vectors).determinant();

  RVE rve{std::move(geometry), std::move(mesh), std::move(assembler), volume};

  auto bcFunctor = homogenization::PBCFunctor<Policy, RVE::point_type>(
      rve.geometry, source_map);

  const Condensation model(std::move(rve), bcFunctor);

  const auto displacement_gradient = Condensation::DisplacementGradient{
      {{0., 0., 0.}, {0., 0., 0.}, {0., 0., 0.}}};

  const auto tangent_matrix = materialmodels::compute_tangent_matrix(
      model, 0, displacement_gradient, 0.);

  auto voigtMatrix = [&]() {
    const int voigtDim = dimension * (dimension + 1) / 2;
    Eigen::Matrix<double, voigtDim, voigtDim> voigtMatrix =
        Eigen::Matrix<double, voigtDim, voigtDim>::Zero();

    auto index = ranges::views::iota(0, dimension);
    for (auto [i, j, k, l] :
         ranges::views::cartesian_product(index, index, index, index)) {
      int dij = i == j;
      int dkl = k == l;
      int p = i * dij + (1 - dij) * (voigtDim - i - j);
      int q = k * dkl + (1 - dkl) * (voigtDim - k - l);
      voigtMatrix(p, q) = tangent_matrix[i][j][k][l];
    }
    return voigtMatrix;
  }();

  std::cout << voigtMatrix << std::endl;
}
