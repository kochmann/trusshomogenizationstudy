// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ae108/cmdline/CommandLineOptionParser.h"
#include "ae108/cpppetsc/Context.h"
#include "ae108/cpppetsc/SequentialComputePolicy.h"
#include "ae108/elements/CoreElement.h"
#include "ae108/elements/embedding/IsoparametricEmbedding.h"
#include "ae108/elements/integrator/IsoparametricIntegrator.h"
#include "ae108/elements/materialmodels/Hookean.h"
#include "ae108/elements/materialmodels/compute_tangent_matrix.h"
#include "ae108/elements/mesh/generate_triangle_mesh.h"
#include "ae108/elements/quadrature/Quadrature.h"
#include "ae108/elements/shape/Tri3.h"
#include "ae108/elements/tensor/as_two_tensor.h"
#include "ae108/homogenization/PBCFunctor.h"
#include "ae108/homogenization/RVEBase.h"
#include "ae108/homogenization/materialmodels/Condensation.h"
#include "mesh/gmsh/GmshContext.h"
#include "mesh/gmsh/extract_mesh_from_gmsh.h"
#include "mesh/gmsh/generate_mesh.h"
#include "mesh/gmsh/set_mesh_granularity.h"

#include <iostream>
#include <random>

using namespace ae108;

using Policy = cpppetsc::SequentialComputePolicy;
using Mesh = cpppetsc::Mesh<Policy>;
using Context = cpppetsc::Context<Policy>;
using Vector = cpppetsc::Vector<Policy>;

namespace embedding = ae108::elements::embedding;
namespace integrator = ae108::elements::integrator;
namespace shape = ae108::elements::shape;
namespace quadrature = ae108::elements::quadrature;
namespace tensor = ae108::elements::tensor;
namespace materialmodels = ae108::elements::materialmodels;

constexpr auto dimension = Mesh::size_type{2};
constexpr auto dof_per_vertex = Mesh::size_type{2};

// First we select a material model and an element type.

using MaterialModel =
    materialmodels::Hookean<dimension, Vector::value_type, Vector::real_type>;
using Shape = shape::Tri3;
using Embedding = embedding::IsoparametricEmbedding<Shape>;
using Quadrature =
    quadrature::Quadrature<quadrature::QuadratureType::Simplex, dimension, 1>;
using Integrator =
    integrator::IsoparametricIntegrator<Shape, Quadrature, Vector::value_type,
                                        Vector::real_type>;
using Element = elements::CoreElement<MaterialModel, Integrator,
                                      Vector::value_type, Vector::real_type>;

using RVE = homogenization::RVEBase<Policy, Element, dimension>;
using Condensation = homogenization::materialmodels::Condensation<RVE>;

int main(int argc, char **argv) {

  const auto context = Context(&argc, &argv);
  const auto gmshContext = mesh::GmshContext(0, 0);

  auto granularity = double{0.5};
  cmdline::CommandLineOptionParser(std::cerr)
      .withOption("granularity,g", "Granularity", &granularity)
      .parse(argc, argv);
  gmsh::open("hexagon.geo");
  mesh::set_mesh_granularity(granularity);
  mesh::generate_mesh(dimension, 1, 6);
  gmsh::write("hexagon.msh");

  const auto [geometry, _, nodeTagToIndex] =
      mesh::extract_mesh_from_gmsh<dimension>();

  auto mesh = RVE::mesh_type::fromConnectivity(
      dimension, geometry.connectivity(), geometry.number_of_positions(),
      dof_per_vertex);

  auto assembler = RVE::assembler_type();

  // We will assign material models randomly. To do so, we need to create two
  // material models and a random number engine. Note that this engine is
  // initialized with the same default seed every time.

  const auto phases = std::array<MaterialModel, 2>{
      {MaterialModel(10.0, 0.), MaterialModel(10.0, 0.)}};
  auto engine = std::default_random_engine();

  // Let's fill the assembler with elements.

  for (const auto &element : mesh.localElements()) {
    Embedding::Collection<Embedding::PhysicalPoint> points;
    std::transform(
        geometry.connectivity().at(element.index()).begin(),
        geometry.connectivity().at(element.index()).end(), points.begin(),
        [&geometry](Mesh::size_type nodeIndex) -> Embedding::PhysicalPoint {
          return geometry.position_of_vertex(nodeIndex);
        });

    // We choose either phase[0] or phase[1] with a probability of 50%

    assembler.emplaceElement(element,
                             phases[std::bernoulli_distribution(.5)(engine)],
                             Integrator(Embedding(points)));
  }

  const double volume = 3. * sqrt(3.) / 2;
  auto rve =
      RVE{std::move(geometry), std::move(mesh), std::move(assembler), volume};

  std::vector<std::vector<std::array<RVE::size_type, 2>>> map = {
      {{13, 17}, {7, 10}, {14, 16}},
      {{13, 15}, {12, 9}, {18, 16}},
      {{15, 17}, {8, 11}, {14, 18}}};

  auto bcFunctor =
      homogenization::PBCFunctor<Policy, RVE::point_type>(rve.geometry, map);
  const auto displacement_gradient =
      Condensation::DisplacementGradient{{{0., 0.}, {0., 0.}}};

  const auto model = Condensation(std::move(rve), std::move(bcFunctor));

  // Finally we compute the tangent matrix and print it to stdout.

  const auto tangent_matrix = materialmodels::compute_tangent_matrix(
      model, 0, displacement_gradient, 0.);

  if (Policy::isPrimaryRank()) {
    std::cout << "Tangent matrix through condensation:" << std::endl;
    std::cout << tensor::as_two_tensor(&tangent_matrix) << std::endl
              << std::endl;
  }
}