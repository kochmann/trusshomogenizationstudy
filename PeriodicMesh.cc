#include "mesh/build_periodic_point_cloud.h"
#include "mesh/construct_voronoi_cell.h"
#include "mesh/gmsh/convert_segment_mesh_to_periodic_volume_mesh.h"
#include "mesh/read_mesh_from_file.h"

constexpr auto Dimension = std::size_t{3};
using Point = std::array<double, Dimension>;

int main(int argc, char **argv) {

  const auto segment_mesh = mesh::read_mesh_from_file<Point>(
      "/mnt/io/unitcells/bitruncatedOctahedron.dat");

  std::vector<double> radii(segment_mesh.connectivity().size(), 0.1);

  const auto lattice_vectors = std::array<Point, Dimension>{
      {{0.5, 0.5, 0.5}, {-0.5, 0.5, 0.5}, {0.5, -0.5, 0.5}}};
  const auto domain = [&lattice_vectors]() {
    const auto origin = Point{0.5, 0.5, 0.5};
    const auto pointCloud = mesh::build_periodic_point_cloud(lattice_vectors);
    return mesh::construct_voronoi_cell(pointCloud, origin);
  }();

  const auto granularity = double{0.02};
  auto mass = double{0.};

  const auto [mesh, source_map] =
      mesh::convert_segment_mesh_to_periodic_volume_mesh(
          segment_mesh, radii, domain, granularity, lattice_vectors, mass);
}
