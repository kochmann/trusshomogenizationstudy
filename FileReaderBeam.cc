#include "ae108/cpppetsc/Context.h"
#include "ae108/cpppetsc/ParallelComputePolicy.h"
#include "ae108/elements/TimoshenkoBeamElement.h"
#include "ae108/elements/materialmodels/compute_tangent_matrix.h"
#include "ae108/elements/tensor/as_matrix_of_rows.h"
#include "ae108/elements/tensor/as_two_tensor.h"
#include "ae108/elements/tensor/as_vector.h"
#include "ae108/homogenization/PBCFunctor.h"
#include "ae108/homogenization/RVEBase.h"
#include "ae108/homogenization/materialmodels/Condensation.h"
#include "mesh/PeriodicDomain.h"
#include "mesh/assign_lattice_to.h"
#include "mesh/build_periodic_point_cloud.h"
#include "mesh/construct_paralleliped_cell.h"
#include "mesh/construct_voronoi_cell.h"
#include "mesh/gmsh/convert_segment_mesh_to_periodic_volume_mesh.h"
#include "mesh/gmsh/utilities.h"
#include "mesh/read_mesh_from_file.h"
#include <Eigen/Dense>
#include <iostream>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/zip.hpp>

#include "fileReader/read_config.h"
#include "fileReader/writer.h"
#include <ae108/cmdline/CommandLineOptionParser.h>

using namespace ae108;

const static Eigen::IOFormat TangentMatrixFormat(Eigen::StreamPrecision,
                                                 Eigen::DontAlignCols, ", ",
                                                 ", ", "", "", "", "\n");

using Policy = ae108::cpppetsc::ParallelComputePolicy;
using Mesh = ae108::cpppetsc::Mesh<Policy>;
using Context = cpppetsc::Context<Policy>;

constexpr auto dimension = Mesh::size_type{3};
constexpr auto number_of_vertices_per_element = Mesh::size_type{2};

using Connectivity =
    std::vector<std::array<std::size_t, number_of_vertices_per_element>>;

using Point = std::array<Mesh::value_type, dimension>;

using VertexPositions = std::vector<Point>;

using LatticeVectors =
    std::array<std::array<Mesh::value_type, dimension>, dimension>;

using Element = ae108::elements::TimoshenkoBeamElement<dimension>;

using Properties =
    ae108::elements::TimoshenkoBeamProperties<Mesh::value_type, dimension>;

using RVE = homogenization::RVEBase<Policy, Element, dimension>;

using Condensation = homogenization::materialmodels::Condensation<RVE>;

constexpr auto young_modulus = Mesh::value_type(1.);
constexpr auto poisson_ratio = Mesh::value_type(0.3);
constexpr auto shear_modulus = young_modulus / (2 * (1 + poisson_ratio));
constexpr auto density = Mesh::value_type(1.);

// Circular cross section
constexpr auto shear_correction_factor =
    (7 + 6 * poisson_ratio) / 6 / (1 + poisson_ratio); // Cowper (1966)

int main(int argc, char **argv) {

  const auto context = Context(&argc, &argv);

  std::string input_folder = "/mnt/io/data";
  std::size_t row = 0;
  std::string output_folder = "/mnt/io/";

  cmdline::CommandLineOptionParser(std::cerr)
      .withOption("id,c", "Specify the row in the dataset.", &row)
      .withOption("input_folder,i", "Specify the location of the dataset.",
                  &input_folder)
      .withOption("output_folder,o", "Specify the location of the output.",
                  &output_folder)
      .parse(argc, argv);

  std::string data_input = input_folder + "/data.csv";
  std::ifstream dataset(data_input);

  const auto config = read_config<dimension>(dataset, row);

  const auto lattice_vectors = config.lattice_vectors;

  std::string name = config.name;

  std::string dat_file = "UC_" + config.number;

  const auto geo = mesh::read_mesh_from_file<Point>(
      input_folder + "/unitcells/" + dat_file + ".dat");

  VertexPositions positions(geo.number_of_positions());
  std::vector<int> indices(positions.size());
  std::iota(indices.begin(), indices.end(), 0);
  for (const auto index : indices)
    positions[index] = geo.position_of_vertex(index);

  const auto element_source =
      mesh::assign_lattice_to(geo.connectivity(), positions, lattice_vectors);

  const auto volume =
      elements::tensor::as_matrix_of_rows(&lattice_vectors).determinant();

  const auto origin = Point{0.5, 0.5, 0.5};

  const auto domain = [](const std::array<Point, dimension> lattice_vectors,
                         const Point &origin) {
    return mesh::construct_paralleliped_cell(lattice_vectors, origin);
  }(lattice_vectors, origin);

  const auto periodicFacePairs = domain.periodicFaces;

  std::vector<std::vector<std::array<int, 2>>> source_map;

  for (const auto &face_pair : periodicFacePairs) {
    std::vector<std::array<int, 2>> node_pairs;

    int target_face = face_pair.first;
    int source_face = face_pair.second;

    const auto translation = mesh::translation_from_to(
        bounding_box_of(domain.vertices_of_face(target_face)),
        bounding_box_of(domain.vertices_of_face(source_face)));

    for (const auto &&[target_index, target] :
         ranges::views::zip(indices, positions))
      for (const auto &&[source_index, source] :
           ranges::views::zip(indices, positions))
        if ((elements::tensor::as_vector(&target) -
             elements::tensor::as_vector(&source) + translation)
                .norm() < 1e-5)
          node_pairs.push_back({target_index, source_index});

    source_map.push_back(node_pairs);
  }

  for (int multiplier = 1; multiplier < 41; multiplier++) {

    double density = multiplier * 0.01;

    auto mesh = RVE::mesh_type::fromConnectivity(dimension, geo.connectivity(),
                                                 geo.number_of_positions(),
                                                 Element::degrees_of_freedom());

    const auto radius = [&]() {
      double weight = 0.;
      for (const auto &element : mesh.localElements()) {
        std::array<typename Mesh::value_type, dimension> element_axis;
        elements::tensor::as_vector(&element_axis) =
            elements::tensor::as_vector(&geo.position_of_vertex(
                geo.connectivity().at(element.index())[1])) -
            elements::tensor::as_vector(&geo.position_of_vertex(
                geo.connectivity().at(element.index())[0]));

        weight += 1. /
                  std::count(element_source.begin(), element_source.end(),
                             element_source[element.index()]) *
                  elements::tensor::as_vector(&element_axis).norm();
      }

      double radius = sqrt((density * volume) / (M_PI * weight));
      return radius;
    }();

    const auto area = radius * radius * M_PI;
    const auto area_moment = M_PI_4 * std::pow(radius, 4);
    const auto polar_moment = M_PI_2 * std::pow(radius, 4);

    auto properties = Properties{young_modulus,
                                 shear_modulus,
                                 shear_correction_factor,
                                 shear_correction_factor,
                                 area,
                                 area_moment,
                                 area_moment,
                                 polar_moment};

    auto assembler = RVE::assembler_type();

    for (const auto &element : mesh.localElements()) {
      std::array<typename Mesh::value_type, dimension> element_axis;
      elements::tensor::as_vector(&element_axis) =
          elements::tensor::as_vector(&geo.position_of_vertex(
              geo.connectivity().at(element.index())[1])) -
          elements::tensor::as_vector(&geo.position_of_vertex(
              geo.connectivity().at(element.index())[0]));

      const auto weight =
          1. / std::count(element_source.begin(), element_source.end(),
                          element_source[element.index()]);
      assembler.emplaceElement(
          element,
          timoshenko_beam_stiffness_matrix(element_axis, properties) * weight);
    }

    RVE rve{std::move(geo), std::move(mesh), std::move(assembler), volume};

    auto bcFunctor = homogenization::PBCFunctor<Policy, RVE::point_type>(
        rve.geometry, source_map);

    const Condensation model(std::move(rve), bcFunctor);

    const auto displacement_gradient =
        Condensation::DisplacementGradient{{{0., 0., 0.},
                                            {0., 0., 0.},
                                            {0., 0., 0.},
                                            {0., 0., 0.},
                                            {0., 0., 0.},
                                            {0., 0., 0.}}};

    const auto tangent_matrix =
        ae108::elements::materialmodels::compute_tangent_matrix(
            model, 0, displacement_gradient, 0.);

    auto voigtMatrix = [&]() {
      const int voigtDim = dimension * (dimension + 1) / 2;
      Eigen::Matrix<double, voigtDim, voigtDim> voigtMatrix =
          Eigen::Matrix<double, voigtDim, voigtDim>::Zero();

      auto index = ranges::views::iota(0, dimension);
      for (auto [i, j, k, l] :
           ranges::views::cartesian_product(index, index, index, index)) {
        int dij = i == j;
        int dkl = k == l;
        int p = i * dij + (1 - dij) * (voigtDim - i - j);
        int q = k * dkl + (1 - dkl) * (voigtDim - k - l);
        voigtMatrix(p, q) = tangent_matrix[i][j][k][l];
      }
      return voigtMatrix;
    }();

    // std::cout << voigtMatrix << std::endl;
    std::string outputFile = output_folder + "/" + name + "_beam.txt";
    writeToFile(outputFile, voigtMatrix, name, 0., density, radius, density);
  }
}
