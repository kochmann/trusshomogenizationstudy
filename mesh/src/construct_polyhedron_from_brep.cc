// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mesh/gmsh/construct_polyhedron_from_brep.h"
#include "ae108/elements/tensor/as_vector.h"
#include "mesh/BoundaryRepresentation.h"
#include <gmsh.h>
#include <map>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/transform.hpp>

namespace mesh {

std::pair<int, int> construct_polyhedron_from_BREP(
    const BoundaryRepresentation<int, double, 3> &brep) noexcept {

  std::map<std::size_t, int> point2tag;
  for (auto &&[index, vertex] : ranges::views::enumerate(brep.vertices))
    point2tag[index] =
        gmsh::model::occ::addPoint(vertex[0], vertex[1], vertex[2]);

  std::map<std::size_t, int> edge2tag;
  for (auto &&[index, edge] : ranges::views::enumerate(brep.edges))
    edge2tag[index] =
        gmsh::model::occ::addLine(point2tag.at(edge[0]), point2tag.at(edge[1]));

  std::vector<int> surfaceTags;
  for (const auto &line_loop : brep.faces)
    surfaceTags.push_back(
        gmsh::model::occ::addPlaneSurface({gmsh::model::occ::addCurveLoop(
            line_loop | ranges::view::transform([&edge2tag](int edge) {
              return edge2tag.at(edge);
            }) |
            ranges::to<std::vector>)}));

  return {3, gmsh::model::occ::addVolume(
                 {gmsh::model::occ::addSurfaceLoop(surfaceTags)})};
}

} // namespace mesh