// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mesh/read_mesh_from_file.h"

namespace mesh {

template ae108::elements::mesh::Mesh<ae108::elements::tensor::Tensor<double, 2>>
read_mesh_from_file(const std::string &filename) noexcept;

template ae108::elements::mesh::Mesh<ae108::elements::tensor::Tensor<double, 3>>
read_mesh_from_file(const std::string &filename) noexcept;

} // namespace mesh