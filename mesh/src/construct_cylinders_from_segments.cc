// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mesh/gmsh/construct_cylinders_from_segments.h"
#include "ae108/elements/tensor/as_vector.h"
#include <Eigen/Dense>
#include <gmsh.h>
#include <range/v3/view/zip.hpp>

namespace mesh {

std::pair<int, int>
cylinder(const ae108::elements::tensor::Tensor<double, 3> &p0,
         const ae108::elements::tensor::Tensor<double, 3> &p1,
         const double radius) {
  const auto axis = [&]() {
    ae108::elements::tensor::Tensor<double, 3> axis;
    ae108::elements::tensor::as_vector(&axis) =
        ae108::elements::tensor::as_vector(&p1) -
        ae108::elements::tensor::as_vector(&p0);
    return axis;
  }();
  return {3, gmsh::model::occ::addCylinder(p0[0], p0[1], p0[2], axis[0],
                                           axis[1], axis[2], radius)};
}

std::pair<int, int>
capped_cylinder(const ae108::elements::tensor::Tensor<double, 3> &p0,
                const ae108::elements::tensor::Tensor<double, 3> &p1,
                const double radius) {
  const auto axis = [&]() {
    ae108::elements::tensor::Tensor<double, 3> axis;
    ae108::elements::tensor::as_vector(&axis) =
        ae108::elements::tensor::as_vector(&p1) -
        ae108::elements::tensor::as_vector(&p0);
    return axis;
  }();

  std::vector<gmsh::vectorpair> temp;
  gmsh::vectorpair cappedCylinder;
  gmsh::model::occ::fuse(
      {{3, gmsh::model::occ::addCylinder(p0[0], p0[1], p0[2], axis[0], axis[1],
                                         axis[2], radius)}},
      {{3, gmsh::model::occ::addSphere(p0[0], p0[1], p0[2], radius)},
       {3, gmsh::model::occ::addSphere(p1[0], p1[1], p1[2], radius)}},
      cappedCylinder, temp);
  return cappedCylinder[0];
}

std::pair<int, int> construct_cylinders_from_segments(
    const ae108::elements::mesh::Mesh<
        ae108::elements::tensor::Tensor<double, 3>> &segment_mesh,
    const std::vector<double> &radii, const bool capped) noexcept {

  using Point = ae108::elements::tensor::Tensor<double, 3>;
  using Mesh = ae108::elements::mesh::Mesh<Point>;

  gmsh::vectorpair cylinder_entities;
  cylinder_entities.reserve(segment_mesh.connectivity().size());
  for (const auto &&[segment, radius] :
       ranges::views::zip(segment_mesh.connectivity(), radii)) {

    const auto &p0 = segment_mesh.position_of_vertex(segment[0]);
    const auto &p1 = segment_mesh.position_of_vertex(segment[1]);

    if (capped)
      cylinder_entities.push_back(capped_cylinder(p0, p1, radius));
    else
      cylinder_entities.push_back(cylinder(p0, p1, radius));
  }

  std::vector<gmsh::vectorpair> temp;
  gmsh::vectorpair fused_entities;
  gmsh::model::occ::fuse(cylinder_entities, cylinder_entities, fused_entities,
                         temp);
  return fused_entities[0];
}

} // namespace mesh