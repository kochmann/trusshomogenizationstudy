// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "mesh/gmsh/extract_mesh_from_gmsh.h"

#include <gmsh.h>
#include <range/v3/algorithm/copy.hpp>
#include <range/v3/view/chunk.hpp>
#include <range/v3/view/enumerate.hpp>

namespace mesh {

template <std::size_t Dimension>
std::tuple<ae108::elements::mesh::Mesh<
               ae108::elements::tensor::Tensor<double, Dimension>>,
           std::map<std::size_t, std::size_t>,
           std::map<std::size_t, std::size_t>>
extract_mesh_from_gmsh() noexcept {

  using Mesh = ae108::elements::mesh::Mesh<
      ae108::elements::tensor::Tensor<double, Dimension>>;

  const auto [nodeTagToIndex, positions] = []() {
    std::vector<std::size_t> nodeTags;
    std::vector<double> nodeCoordinates, parametricNodeCoordinates;
    gmsh::model::mesh::getNodes(nodeTags, nodeCoordinates,
                                parametricNodeCoordinates, Dimension, -1, true,
                                false);

    const auto numberOfNodes = nodeTags.size();

    auto nodeTagToIndex = std::map<std::size_t, std::size_t>();
    auto positions = typename Mesh::Positions(numberOfNodes);
    for (auto &&[index, coordinates] :
         ranges::views::enumerate(nodeCoordinates | ranges::view::chunk(3))) {
      nodeTagToIndex[nodeTags.at(index)] = index;
      ranges::copy(coordinates, positions.at(index).begin());
    }
    return std::make_tuple(nodeTagToIndex, positions);
  }();

  const auto [elementTagToIndex, connectivity] = [&nodeTagToIndex]() {
    std::vector<int> elementTypes;
    gmsh::model::mesh::getElementTypes(elementTypes, Dimension);

    std::vector<std::vector<std::size_t>> elementTypeTags, elementNodeTags;
    gmsh::model::mesh::getElements(elementTypes, elementTypeTags,
                                   elementNodeTags, Dimension);

    const auto numberOfElements = [&elementTypeTags]() {
      auto numberOfElements = std::size_t{0};
      for (const auto &elementTags : elementTypeTags)
        numberOfElements += elementTags.size();
      return numberOfElements;
    }();

    auto elementTagToIndex = std::map<std::size_t, std::size_t>();
    auto connectivity = typename Mesh::Connectivity();
    connectivity.reserve(numberOfElements);
    for (const auto &[elementType, elementTags] :
         ranges::views::zip(elementTypes, elementTypeTags))
      for (const auto &elementTag : elementTags) {

        std::vector<std::size_t> nodeTags;
        gmsh::model::mesh::getElement(elementTag, elementType, nodeTags);

        std::vector<std::size_t> nodeIndices(nodeTags.size());
        for (auto &&[index, nodeTag] : ranges::views::enumerate(nodeTags))
          nodeIndices[index] = nodeTagToIndex.at(nodeTag);

        elementTagToIndex[elementTag] = connectivity.size();
        connectivity.push_back(nodeIndices);
      }
    return std::make_tuple(elementTagToIndex, connectivity);
  }();

  return {Mesh(connectivity, positions), elementTagToIndex, nodeTagToIndex};
}

template std::tuple<
    ae108::elements::mesh::Mesh<ae108::elements::tensor::Tensor<double, 3>>,
    std::map<std::size_t, std::size_t>, std::map<std::size_t, std::size_t>>
extract_mesh_from_gmsh<3>() noexcept;

template std::tuple<
    ae108::elements::mesh::Mesh<ae108::elements::tensor::Tensor<double, 2>>,
    std::map<std::size_t, std::size_t>, std::map<std::size_t, std::size_t>>
extract_mesh_from_gmsh<2>() noexcept;

} // namespace mesh