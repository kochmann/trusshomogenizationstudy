// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>

namespace mesh {

// see https://en.wikipedia.org/wiki/Cartesian_product
template <class value_type, std::size_t I, std::size_t J, std::size_t K,
          std::size_t L>
std::array<std::array<value_type, I + K>, J * L> cartesian_product(
    const std::array<std::array<value_type, I>, J> &set_A,
    const std::array<std::array<value_type, K>, L> &set_B) noexcept {
  std::array<std::array<value_type, I + K>, J * L> ordered_pairs;
  for (size_t j = 0; j < J; j++)
    for (size_t l = 0; l < L; l++) {
      std::copy(set_A[j].begin(), set_A[j].end(),
                ordered_pairs[j * L + l].begin());
      std::copy(set_B[l].begin(), set_B[l].end(),
                (ordered_pairs[j * L + l].begin() + I));
    }
  return ordered_pairs;
}

} // namespace mesh