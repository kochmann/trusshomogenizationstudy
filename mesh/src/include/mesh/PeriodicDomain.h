#pragma once

#include "mesh/BoundaryRepresentation.h"
#include "mesh/bounding_box_of_vertices.h"

struct PeriodicDomain : BoundaryRepresentation<int, double, 3> {

  std::vector<std::pair<BoundingBox<Point>, BoundingBox<Point>>>
  periodic_boundaries() const noexcept {
    std::vector<std::pair<BoundingBox<Point>, BoundingBox<Point>>>
        periodic_boundaries;
    for (const auto &pair : periodicFaces)
      periodic_boundaries.push_back(
          {bounding_box_of(this->vertices_of_face(pair.first)),
           bounding_box_of(this->vertices_of_face(pair.second))});
    return periodic_boundaries;
  };

  std::vector<std::pair<int, int>> periodicFaces;
};
