#pragma once

#include <array>
#include <voro++.hh>

#include "mesh/extract_voronoi_cell.h"

namespace mesh {
template <class Point>
PeriodicDomain construct_voronoi_cell(std::vector<Point> _point_cloud,
                                      Point origin) {

  voro::voronoicell voronoi_cell;
  const auto bound = 1e3;
  voronoi_cell.init(-bound, bound, -bound, bound, -bound, bound);

  for (auto &point : _point_cloud)
    voronoi_cell.plane(point[0], point[1], point[2]);

  return extract_brep(voronoi_cell, origin);
}
} // namespace mesh