// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/tensor/as_matrix_of_rows.h"
#include "ae108/elements/tensor/as_vector.h"
#include "mesh/static_power.h"
#include <algorithm>
#include <vector>

namespace mesh {

template <class size_type, class Point>
std::vector<Point> get_positions(const std::vector<Point> &vertex_positions,
                                 const std::vector<size_type> &connectivity) {
  std::vector<Point> positions(connectivity.size());
  std::transform(
      connectivity.begin(), connectivity.end(), positions.begin(),
      [&vertex_positions](size_type index) { return vertex_positions[index]; });
  std::sort(positions.begin(), positions.end());
  return positions;
}

template <class size_type, class value_type, size_type dimension_>
size_type find_source_of_target(
    const std::vector<std::array<value_type, dimension_>> &positions,
    const std::vector<std::vector<size_type>> &connectivity,
    const std::array<std::array<value_type, dimension_>, dimension_>
        &lattice_vectors,
    const std::array<std::array<value_type, dimension_>, power(3, dimension_)>
        &combinations,
    const size_type &target) {

  const auto target_positions = get_positions(positions, connectivity[target]);

  std::array<value_type, dimension_> translation;
  for (size_type source = 0; source < target; source++) {
    const auto source_positions =
        get_positions(positions, connectivity[source]);
    for (const auto &combination : combinations) {
      ae108::elements::tensor::as_vector(&translation) =
          ae108::elements::tensor::as_matrix_of_rows(&lattice_vectors) *
          ae108::elements::tensor::as_vector(&combination);

      if (std::equal(
              target_positions.begin(), target_positions.end(),
              source_positions.begin(),
              [&translation](std::array<value_type, dimension_> point1,
                             std::array<value_type, dimension_> point2) {
                return ae108::elements::tensor::as_vector(&point2).isApprox(
                    ae108::elements::tensor::as_vector(&point1) +
                    ae108::elements::tensor::as_vector(&translation));
              }))
        return source;
    }
  }
  return target;
}

} // namespace mesh
