#pragma once

#include "ae108/elements/tensor/as_vector.h"
#include "mesh/PeriodicDomain.h"
#include <range/v3/action/sort.hpp>
#include <range/v3/algorithm/copy.hpp>
#include <range/v3/algorithm/sort.hpp>
#include <range/v3/view/chunk.hpp>
#include <range/v3/view/enumerate.hpp>
#include <voro++.hh>

PeriodicDomain extract_brep(voro::voronoicell &voronoiCell,
                            const PeriodicDomain::Point &origin)

{
  using ae108::elements::tensor::as_vector;
  namespace rv = ranges::views;

  using Cell = PeriodicDomain;

  auto add_edge = [](const Cell::Edge &edge, Cell::Edges &edges) {
    for (auto [index, connectivity] : rv::enumerate(edges))
      if (edge == connectivity)
        return index;
    edges.push_back(edge);
    return edges.size() - 1;
  };

  const auto vertices = [](voro::voronoicell &voronoicell) {
    std::vector<double> vertices;
    voronoicell.vertices(vertices);
    return vertices;
  }(voronoiCell);

  const auto faces = [](voro::voronoicell &voronoicell) {
    std::vector<std::vector<std::array<int, 2>>> faces;
    std::vector<int> temp;
    voronoicell.face_vertices(temp);
    for (int i = 0; i < temp.size(); i = i + temp[i] + 1) {
      std::vector<std::array<int, 2>> edges;
      for (int j = 1; j < temp[i]; j++) {
        edges.push_back({temp[i + j], temp[i + j + 1]});
        ranges::sort(edges.back());
      }
      edges.push_back({temp[i + temp[i]], temp[i + 1]});
      ranges::sort(edges.back());
      faces.push_back(edges);
    }
    return faces;
  }(voronoiCell);

  const auto facesNormals = [](voro::voronoicell &voronoicell) {
    std::vector<Cell::Point> facesNormals(voronoicell.number_of_faces());
    std::vector<double> temp;
    voronoicell.normals(temp);
    for (const auto &[index, normal] : rv::enumerate(temp | rv::chunk(3)))
      ranges::copy(normal, facesNormals[index].begin());
    return facesNormals;
  }(voronoiCell);

  PeriodicDomain cell;

  cell.faces.resize(voronoiCell.number_of_faces());
  for (const auto &[index, face] : rv::enumerate(faces))
    for (auto &edge : face)
      cell.faces[index].push_back(add_edge(edge, cell.edges));

  cell.vertices.resize(vertices.size() / 3);
  for (const auto &[index, vertex] : rv::enumerate(vertices | rv::chunk(3)))
    ranges::copy(vertex, cell.vertices[index].begin());

  for (auto &vertex : cell.vertices)
    as_vector(&vertex) += as_vector(&origin);

  for (std::size_t i = 0; i < facesNormals.size(); i++)
    for (std::size_t j = i + 1; j < facesNormals.size(); j++)
      if (as_vector(&facesNormals[i])
              .cross(as_vector(&facesNormals[j]))
              .isZero())
        cell.periodicFaces.push_back({i, j});

  return cell;
}