// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/tensor/as_vector.h"
#include "mesh/gmsh/utilities.h"
#include <Eigen/Dense>
#include <gmsh.h>

namespace mesh {

void heal_periodic_surface(const std::pair<int, int> &sourceSurface,
                           const std::pair<int, int> &targetSurface,
                           const Eigen::Vector3d &translation,
                           const double tol = 1e-3) {
  for (const auto &source : points_in(sourceSurface)) {

    const auto query = (coords_of(source) + translation).eval();

    bool targetFound = false;
    for (const auto &target : points_in(targetSurface))
      targetFound += coords_of(target).isApprox(query, tol);

    if (!targetFound)
      add_point(query);
  }
};

template <class BoundingBox>
void heal_periodic_domains(
    const std::vector<std::pair<BoundingBox, BoundingBox>> &periodicDomainPairs,
    const double tol = 1e-9) noexcept {

  for (const auto &domainPair : periodicDomainPairs)
    for (const auto &source : surfaces_in(domainPair.first))
      for (const auto &target : surfaces_in(domainPair.second)) {
        const auto translation =
            translation_from_to(domainPair.first, domainPair.second);
        if (centroid_of(target).isApprox(centroid_of(source) + translation,
                                         tol)) {
          heal_periodic_surface(source, target, translation);
          heal_periodic_surface(target, source, -translation);
        }
      }
}

} // namespace mesh