// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "mesh/PeriodicDomain.h"
#include "mesh/bounding_box_of_vertices.h"
#include "mesh/gmsh/GmshContext.h"
#include "mesh/gmsh/construct_cylinders_from_segments.h"
#include "mesh/gmsh/construct_polyhedron_from_brep.h"
#include "mesh/gmsh/extract_mesh_from_gmsh.h"
#include "mesh/gmsh/generate_mesh.h"
#include "mesh/gmsh/get_periodicity_map.h"
#include "mesh/gmsh/heal_periodic_domains.h"
#include "mesh/gmsh/set_domain_boundary_periodic.h"
#include "mesh/gmsh/set_mesh_granularity.h"
#include <iostream>
namespace mesh {

template <std::size_t Dimension>
std::pair<ae108::elements::mesh::Mesh<std::array<double, Dimension>>,
          std::vector<std::vector<std::array<int, 2>>>>
convert_segment_mesh_to_periodic_volume_mesh(
    const ae108::elements::mesh::Mesh<std::array<double, Dimension>>
        &segment_mesh,
    const std::vector<double> radii, const PeriodicDomain rveDomain,
    const double granularity,
    const std::array<std::array<double, Dimension>, Dimension> lattice_vectors,
    double &mass, const int order = 1, const int algorithm = 6,
    const int verbosity = 0) noexcept {
  const auto gmshContext = mesh::GmshContext(0, 0);
  gmsh::option::setNumber("General.Verbosity", verbosity);
  gmsh::option::setNumber("Geometry.OCCBoundsUseStl", 1);

  gmsh::vectorpair rveEntity;
  std::vector<gmsh::vectorpair> scratch;
  gmsh::model::occ::intersect(
      {mesh::construct_cylinders_from_segments(segment_mesh, radii, true)},
      {mesh::construct_polyhedron_from_BREP(rveDomain)}, rveEntity, scratch);

  gmsh::model::occ::synchronize();

  gmsh::model::occ::getMass(rveEntity[0].first, rveEntity[0].second, mass);

  mesh::heal_periodic_domains(rveDomain.periodic_boundaries());
  gmsh::model::occ::synchronize();

  mesh::set_domain_boundary_periodic(rveDomain.periodic_boundaries());

  mesh::set_mesh_granularity(granularity);

  mesh::generate_mesh(Dimension, order, algorithm);
  gmsh::write("mesh.msh");

  const auto [volume_mesh, _, nodeTagToIndex] =
      mesh::extract_mesh_from_gmsh<Dimension>();

  const auto source_map = mesh::get_periodicity_map(
      rveDomain.periodic_boundaries(), nodeTagToIndex);
  return {volume_mesh, source_map};
}

} // namespace mesh