// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/tensor/as_vector.h"
#include "mesh/gmsh/utilities.h"
#include <Eigen/Dense>
#include <gmsh.h>

namespace mesh {

template <class BoundingBox>
void set_domain_boundary_periodic(
    const std::vector<std::pair<BoundingBox, BoundingBox>> &periodicDomainPairs,
    const double tol = 1e-9) noexcept {

  for (const auto &domainPair : periodicDomainPairs)
    for (const auto &source : surfaces_in(domainPair.first))
      for (const auto &target : surfaces_in(domainPair.second)) {
        const auto tranlation =
            translation_from_to(domainPair.first, domainPair.second);
        if (centroid_of(target).isApprox(centroid_of(source) + tranlation, tol))
          gmsh::model::mesh::setPeriodic(2, {target.second}, {source.second},
                                         as_transform(tranlation));
      }
}

} // namespace mesh