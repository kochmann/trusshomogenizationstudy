// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/tensor/as_vector.h"
#include <Eigen/Dense>
#include <array>
#include <gmsh.h>
#include <iostream>
#include <numeric>
#include <range/v3/view/zip.hpp>
#include <set>

namespace mesh {

std::vector<double> as_transform(const Eigen::Vector3d translation) noexcept {
  const auto transform =
      Eigen::Transform<double, 3, Eigen::Affine, Eigen::RowMajor>(
          Eigen::Translation3d(translation));
  return std::vector<double>(transform.data(),
                             transform.data() + transform.matrix().size());
};

Eigen::Vector3d centroid_of(const std::pair<int, int> &entity) noexcept {
  Eigen::Vector3d centroid;
  gmsh::model::occ::getCenterOfMass(entity.first, entity.second, centroid[0],
                                    centroid[1], centroid[2]);
  return centroid;
};

template <class BoundingBox>
gmsh::vectorpair surfaces_in(const BoundingBox &box, const double tol = 1e-3) {
  gmsh::vectorpair surfaces;
  gmsh::model::getEntitiesInBoundingBox(
      box.min[0] - tol, box.min[1] - tol, box.min[2] - tol, box.max[0] + tol,
      box.max[1] + tol, box.max[2] + tol, surfaces, 2);
  return surfaces;
};

template <class BoundingBox>
Eigen::Vector3d translation_from_to(const BoundingBox &from,
                                    const BoundingBox &to,
                                    const double tol = 1e-3) {
  assert((ae108::elements::tensor::as_vector(&to.max) -
          ae108::elements::tensor::as_vector(&to.min))
             .isApprox(ae108::elements::tensor::as_vector(&from.max) -
                           ae108::elements::tensor::as_vector(&from.min),
                       tol)); // from and to have equal size
  return (ae108::elements::tensor::as_vector(&to.min) -
          ae108::elements::tensor::as_vector(&from.min))
      .eval();
};

std::set<std::pair<int, int>> points_in(const std::pair<int, int> &surface) {
  gmsh::vectorpair points;
  std::vector<int> volumes, edges;
  gmsh::model::getAdjacencies(surface.first, surface.second, volumes, edges);
  for (const auto &edge : edges) {
    std::vector<int> surfaces, vertices;
    gmsh::model::getAdjacencies(1, edge, surfaces, vertices);
    for (const auto &vertex : vertices) {
      std::vector<double> a, b;
      gmsh::model::getValue(0, vertex, a, b);
      points.push_back({0, vertex});
    }
  }
  return std::set(points.begin(), points.end());
};

Eigen::Vector3d coords_of(const std::pair<int, int> &point) {
  std::vector<double> a, b;
  gmsh::model::getValue(point.first, point.second, a, b);
  return Eigen::Vector3d(b.data());
};

void add_point(const Eigen::Vector3d &point) {
  gmsh::vectorpair volumeEntity;
  gmsh::model::occ::getEntities(volumeEntity, 3);
  assert(volumeEntity.size() == 1);

  gmsh::vectorpair pointEntity{
      {0, gmsh::model::occ::addPoint(point[0], point[1], point[2])}};

  gmsh::vectorpair ov;
  std::vector<gmsh::vectorpair> ovv;
  gmsh::model::occ::fragment(volumeEntity, pointEntity, ov, ovv);
};

} // namespace mesh