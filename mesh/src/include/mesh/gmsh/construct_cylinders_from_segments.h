// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/mesh/Mesh.h"
#include "ae108/elements/tensor/Tensor.h"
#include <vector>

namespace mesh {

/**
 * @brief Converts a segment mesh with segments of circular cross-section into a
 * volume mesh with circular cross-section using gmsh
 *
 * @param segment_mesh A mesh containing segments.
 * @param radii A vector containing the radius of each segment.
 */
std::pair<int, int> construct_cylinders_from_segments(
    const ae108::elements::mesh::Mesh<
        ae108::elements::tensor::Tensor<double, 3>> &segment_mesh,
    const std::vector<double> &radii, const bool capped = true) noexcept;

} // namespace mesh