// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "mesh/BoundaryRepresentation.h"

namespace mesh {

/**
 * @brief Converts a segment mesh with segments of circular cross-section into a
 * volume mesh with circular cross-section using gmsh
 *
 * @param brep A boundary representation.
 */
std::pair<int, int> construct_polyhedron_from_BREP(
    const BoundaryRepresentation<int, double, 3> &brep) noexcept;

} // namespace mesh