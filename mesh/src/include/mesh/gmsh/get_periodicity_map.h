// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/tensor/as_vector.h"
#include <Eigen/Dense>
#include <gmsh.h>
#include <map>
#include <numeric>
#include <range/v3/view/zip.hpp>

namespace mesh {

/**
 * @brief Creates a simple mesh of cuboid shape by splitting
 * into smaller cuboids as defined by `granularity`.
 *
 * @param size The vertex of the cuboid that is opposite to [0., 0., 0.].
 * @param granularity The number of cuboids along the three axes.
 */
template <class BoundingBox>
std::vector<std::vector<std::array<int, 2>>> get_periodicity_map(
    const std::vector<std::pair<BoundingBox, BoundingBox>> &periodicDomainPairs,
    const std::map<std::size_t, std::size_t> nodeTagToPositionIndex) {

  auto surfaces_in = [](const BoundingBox &box, const double tol = 1e-3) {
    gmsh::vectorpair surfaces;
    gmsh::model::getEntitiesInBoundingBox(
        box.min[0] - tol, box.min[1] - tol, box.min[2] - tol, box.max[0] + tol,
        box.max[1] + tol, box.max[2] + tol, surfaces, 2);
    return surfaces;
  };

  const auto numberOfNodes = []() {
    std::vector<std::size_t> nodeTags;
    std::vector<double> nodeCoordinates, parametricNodeCoordinates;
    gmsh::model::mesh::getNodes(nodeTags, nodeCoordinates,
                                parametricNodeCoordinates, 3, -1, true, false);
    return nodeTags.size();
  }();
  assert(numberOfNodes > 0); // mesh has been generated successfully

  std::vector<std::vector<std::array<int, 2>>> periodic_pairs;
  for (const auto &domainPair : periodicDomainPairs) {
    std::vector<std::array<int, 2>> node_pairs;
    for (const auto &target_surface : surfaces_in(domainPair.second)) {

      const auto [targetNodeTags,
                  sourceNodeTags] = [](const std::pair<int, int> &target) {
        int source;
        std::vector<size_t> targetNodeTags, sourceNodeTags;
        std::vector<double> _;
        gmsh::model::mesh::getPeriodicNodes(target.first, target.second, source,
                                            targetNodeTags, sourceNodeTags, _);
        return std::make_tuple(targetNodeTags, sourceNodeTags);
      }(target_surface);

      for (const auto &&[sourceTag, targetTag] :
           ranges::views::zip(sourceNodeTags, targetNodeTags))
        node_pairs.push_back({nodeTagToPositionIndex.at(targetTag),
                              nodeTagToPositionIndex.at(sourceTag)});
    }
    periodic_pairs.push_back(node_pairs);
  }

  return periodic_pairs;
}

} // namespace mesh