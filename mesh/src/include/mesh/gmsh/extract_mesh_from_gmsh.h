// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/mesh/Mesh.h"
#include "ae108/elements/tensor/Tensor.h"
#include <map>
#include <vector>

namespace mesh {

/**
 * @brief Extract mesh from Gmsh.
 *
 */
template <std::size_t Dimension>
std::tuple<ae108::elements::mesh::Mesh<
               ae108::elements::tensor::Tensor<double, Dimension>>,
           std::map<std::size_t, std::size_t>,
           std::map<std::size_t, std::size_t>>
extract_mesh_from_gmsh() noexcept;

} // namespace mesh