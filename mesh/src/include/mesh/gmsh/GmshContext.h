// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <gmsh.h>
#include <memory>

namespace mesh {

class GmshContext {
public:
  /**
   * @brief Initializes Gmsh. Gmsh is finalized automatically when the
   * instance of GmshContext is destroyed.
   *
   * @param argc `argc` as provided to main().
   * @param argv `argv` as provided to main().
   * @param readConfigFiles Option to read config file.
   */
  explicit GmshContext(int const argc, char **const argv,
                       const bool readConfigFiles = true);

private:
  /**
   * @brief Initializes Gmsh.
   */
  static void initialize(int const argc, char **const argv,
                         const bool readConfigFiles = true);

  /**
   * @brief Finalizes Gmsh.
   */
  static void finalize(void *);

  using Token = std::unique_ptr<GmshContext, decltype(&finalize)>;
  Token token_;
};

} // namespace mesh
namespace mesh {

GmshContext::GmshContext(int const argc, char **const argv,
                         const bool readConfigFiles)
    : token_((initialize(argc, argv, readConfigFiles), this), &finalize) {}

void GmshContext::initialize(int const argc, char **const argv,
                             const bool readConfigFiles) {
  gmsh::initialize(argc, argv, readConfigFiles);
}

void GmshContext::finalize(void *) { gmsh::finalize(); }

} // namespace mesh