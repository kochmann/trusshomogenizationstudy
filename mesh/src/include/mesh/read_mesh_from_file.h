// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "ae108/elements/mesh/Mesh.h"
#include "ae108/elements/tensor/Tensor.h"
#include <fstream>
#include <sstream>

namespace mesh {

/**
 * @brief Reads a mesh from a file.
 * *
 * @param filename Name of file with mesh
 */
template <class Point>
ae108::elements::mesh::Mesh<Point>
read_mesh_from_file(const std::string &filename) noexcept;

extern template ae108::elements::mesh::Mesh<
    ae108::elements::tensor::Tensor<double, 2>>
read_mesh_from_file(const std::string &filename) noexcept;

extern template ae108::elements::mesh::Mesh<
    ae108::elements::tensor::Tensor<double, 3>>
read_mesh_from_file(const std::string &filename) noexcept;

} // namespace mesh

namespace mesh {

template <class Point>
ae108::elements::mesh::Mesh<Point>
read_mesh_from_file(const std::string &filename) noexcept {

  using Mesh = ae108::elements::mesh::Mesh<Point>;
  typename Mesh::Positions positions;
  typename Mesh::Connectivity connectivity;

  bool doingPoints = true;
  std::ifstream file(filename);
  std::string line;
  while (std::getline(file, line)) {
    if (line.find("connectivity") == 0)
      doingPoints = false;
    else {
      std::stringstream linestream(line);
      std::string value;
      if (doingPoints) {
        Point point;
        std::size_t i = 0;
        while (std::getline(linestream, value, ','))
          point[i++] = std::atof(value.c_str());
        positions.push_back(point);
      } else {
        std::vector<std::size_t> conn;
        while (std::getline(linestream, value, ','))
          conn.push_back(std::atof(value.c_str()));
        connectivity.push_back(conn);
      }
    }
  }
  file.close();

  return Mesh(connectivity, positions);
}

} // namespace mesh