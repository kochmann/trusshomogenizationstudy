// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>

#include "mesh/cartesian_product.h"
#include "mesh/create_symmetric_sequence.h"
#include "mesh/static_power.h"

namespace mesh {

template <class value_type, std::size_t dimension_, std::size_t n = 1>
std::array<std::array<value_type, dimension_>, power(2 * n + 1, dimension_)>
lattice_vector_multiplier() noexcept {
  exit(1);
}

template <>
inline std::array<std::array<double, 2>, power(2 * 1 + 1, 2)>
lattice_vector_multiplier() noexcept {
  return cartesian_product(create_symmetric_sequence<double, std::size_t(1)>(),
                           create_symmetric_sequence<double, std::size_t(1)>());
}
template <>
inline std::array<std::array<double, 3>, power(2 * 1 + 1, 3)>
lattice_vector_multiplier() noexcept {
  return cartesian_product(
      create_symmetric_sequence<double, std::size_t(1)>(),
      cartesian_product(create_symmetric_sequence<double, std::size_t(1)>(),
                        create_symmetric_sequence<double, std::size_t(1)>()));
}

} // namespace mesh