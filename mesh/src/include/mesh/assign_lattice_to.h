// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "mesh/find_source_of_target.h"
#include "mesh/lattice_vector_multiplier.h"
#include <vector>

namespace mesh {

template <class value_type, std::size_t dimension_>
std::vector<std::size_t> assign_lattice_to(
    const std::vector<std::vector<std::size_t>> &connectivities,
    const std::vector<std::array<value_type, dimension_>> &positions,
    const std::array<std::array<value_type, dimension_>, dimension_>
        &lattice_vectors) noexcept {

  const auto multiplier = lattice_vector_multiplier<double, dimension_, 1>();

  std::vector<std::size_t> source(connectivities.size());
  for (std::size_t target = 0; target < connectivities.size(); target++)
    source[target] = find_source_of_target(positions, connectivities,
                                           lattice_vectors, multiplier, target);
  return source;
}

template <class value_type, std::size_t dimension_>
std::vector<std::size_t> assign_lattice_to(
    const std::vector<std::array<value_type, dimension_>> &positions,
    const std::array<std::array<value_type, dimension_>, dimension_>
        &lattice_vectors) noexcept {

  auto connectivities = std::vector<std::vector<std::size_t>>(positions.size());
  std::size_t i = 0;
  std::for_each(connectivities.begin(), connectivities.end(),
                [&i](std::vector<std::size_t> &n) { n.push_back(i++); });

  return assign_lattice_to(connectivities, positions, lattice_vectors);
}

} // namespace mesh
