// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <algorithm>
#include <array>

namespace mesh {

template <class value_type, std::size_t n = 1>
std::array<std::array<value_type, 1>, 2 * n + 1> create_symmetric_sequence() {
  std::array<std::array<value_type, 1>, 2 * n + 1> sequence;
  auto i = -1. * (value_type)n;
  std::for_each(sequence.begin(), sequence.end(),
                [&i](std::array<value_type, 1> &s) { s = {i++}; });
  return sequence;
}

} // namespace mesh