#pragma once

#include <array>
#include <vector>

#include "ae108/elements/tensor/as_matrix_of_columns.h"
#include "ae108/elements/tensor/as_vector.h"
#include "mesh/PeriodicDomain.h"
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/enumerate.hpp>
#include <range/v3/view/zip.hpp>

namespace mesh {
template <class Point, std::size_t Dimension>
PeriodicDomain
construct_paralleliped_cell(const std::array<Point, Dimension> &lattice_vectors,
                            Point origin) {

  PeriodicDomain unitcell;

  using Vertices = std::vector<Point>;
  using Edge = std::array<std::size_t, 2>;
  using Edges = std::vector<Edge>;
  using Faces = std::vector<std::vector<std::size_t>>;

  namespace rv = ranges::views;
  using ae108::elements::tensor::as_matrix_of_columns;
  using ae108::elements::tensor::as_vector;

  Point start_corner;
  Point offset;
  as_vector(&offset) =
      0.5 * (as_vector(&lattice_vectors[0]) + as_vector(&lattice_vectors[1]) +
             as_vector(&lattice_vectors[2]));
  as_vector(&start_corner) = as_vector(&origin) - as_vector(&offset);

  constexpr auto get_array = [](auto &&...x) {
    return std::array{std::forward<decltype(x)>(x)...};
  };

  std::vector<double> set = {0., 1.};
  const auto permutations = rv::cartesian_product(set, set, set);

  for (auto [index, tuple] : rv::enumerate(permutations)) {
    const auto permutation = std::apply(get_array, tuple);
    Point point;
    as_vector(&point) =
        as_vector(&start_corner) +
        (as_matrix_of_columns(&lattice_vectors) * as_vector(&permutation));

    unitcell.vertices.push_back(point);
  }

  unitcell.edges = {{0, 1}, {2, 3}, {4, 5}, {6, 7}, {0, 4}, {1, 5},
                    {2, 6}, {3, 7}, {0, 2}, {1, 3}, {4, 6}, {5, 7}};
  unitcell.faces = {{0, 8, 1, 9}, {10, 3, 11, 2}, {0, 4, 2, 5},
                    {6, 3, 7, 1}, {6, 10, 4, 8},  {7, 11, 5, 9}};
  unitcell.periodicFaces = {{0, 1}, {2, 3}, {4, 5}};

  return unitcell;
}
} // namespace mesh