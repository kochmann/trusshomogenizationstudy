// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <array>
#include <vector>

template <class SizeType_, class ValueType_, SizeType_ Dimension_>
struct BoundaryRepresentation {
  using Point = std::array<ValueType_, Dimension_>;
  using Vertices = std::vector<Point>;
  using Edge = std::array<SizeType_, 2>;
  using Edges = std::vector<Edge>;
  using Faces = std::vector<std::vector<SizeType_>>;

  static constexpr SizeType_ dimension() noexcept { return Dimension_; }

  std::vector<Point> vertices_of_edge(SizeType_ edge) const noexcept {
    std::vector<Point> edge_vertices;
    for (const auto &vertex : edges[edge])
      edge_vertices.push_back(vertices[vertex]);
    return edge_vertices;
  };

  std::vector<Point> vertices_of_face(SizeType_ index) const noexcept {
    std::vector<Point> face_vertices;
    for (const auto &edge : faces[index]) {
      const auto edge_vertices = vertices_of_edge(edge);
      face_vertices.insert(face_vertices.end(), edge_vertices.begin(),
                           edge_vertices.end());
    }
    return face_vertices;
  };

  Vertices vertices;
  Edges edges;
  Faces faces;
};