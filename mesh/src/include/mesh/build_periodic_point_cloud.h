// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <array>
#include <vector>

#include "ae108/elements/tensor/as_matrix_of_rows.h"
#include "ae108/elements/tensor/as_vector.h"
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/zip.hpp>

namespace mesh {
template <class PointType, std::size_t Dimension>
std::vector<PointType> build_periodic_point_cloud(
    const std::array<PointType, Dimension> &lattice_vectors,
    PointType origin = {0, 0, 0}, std::vector<double> set = {-1, 0, 1}) {

  namespace rv = ranges::views;
  using ae108::elements::tensor::as_matrix_of_rows;
  using ae108::elements::tensor::as_vector;

  constexpr auto get_array = [](auto &&...x) {
    return std::array{std::forward<decltype(x)>(x)...};
  };

  std::vector<PointType> pointCloud(std::pow(set.size(), Dimension));
  const auto permutations = rv::cartesian_product(set, set, set);

  for (auto &&[point, tuple] : rv::zip(pointCloud, permutations)) {
    const auto permutation = std::apply(get_array, tuple);
    as_vector(&point) =
        as_vector(&origin) +
        (as_matrix_of_rows(&lattice_vectors) * as_vector(&permutation));
  }

  return pointCloud;
}
} // namespace mesh