// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "mesh/BoundingBox.h"
#include <vector>

template <class Point>
BoundingBox<Point> bounding_box_of(const std::vector<Point> &vertices,
                                   double tol = 1e-6) {
  Point min(vertices[0]);
  Point max(vertices[0]);
  for (const auto &vertex : vertices)
    for (int i = 0; i < vertex.size(); i++) {
      min[i] = std::min(vertex[i], min[i]);
      max[i] = std::max(vertex[i], max[i]);
    }

  // auto minmax = std::minmax_element(
  //     vertices.begin(), vertices.end(), [&tol](const auto &a, const auto &b)
  //     {
  //       return std::lexicographical_compare(
  //           a.begin(), a.end(), b.begin(), b.end(),
  //           [&tol](const auto &x, const auto &y) { return (x - y) < -tol; });
  //     });
  // return {*minmax.first, *minmax.second};
  return {min, max};
};