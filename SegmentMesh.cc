// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ae108/elements/mesh/Mesh.h"
#include "mesh/gmsh/GmshContext.h"
#include "mesh/gmsh/construct_cylinders_from_segments.h"
#include "mesh/gmsh/extract_mesh_from_gmsh.h"
#include "mesh/gmsh/generate_mesh.h"
#include "mesh/gmsh/set_mesh_granularity.h"

constexpr std::size_t dimension = 3;
using Point = std::array<double, dimension>;

int main(int argc, char **argv) {

  const auto gmshContext = mesh::GmshContext(argc, argv);

  const ae108::elements::mesh::Mesh<Point> segment_mesh{
      {{0, 1}, {1, 2}, {2, 3}, {3, 0}},
      {{0., 0., 0.}, {1., 0., 0.}, {1., 1., 0.}, {0., 1., 0.}}};

  std::vector<double> radii({0.03, 0.04, 0.05, 0.06});

  mesh::construct_cylinders_from_segments(segment_mesh, radii, true);

  gmsh::write("square.step");

  mesh::set_mesh_granularity(0.025);

  const int order = 1;
  mesh::generate_mesh(dimension, order, 6);

  gmsh::write("square.vtk");

  const auto cylinder_mesh = mesh::extract_mesh_from_gmsh<dimension>();
}