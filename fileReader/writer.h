#pragma once
#include <Eigen/Dense>
#include <fstream>
#include <iomanip>
#include <limits>
#include <string>
#include <vector>

///
/// Writes the contents of the vector 'data' to the textfile filename
/// The output format should be load-able in MATLAB and Numpy using the
/// load-command.
///
template <int VoigtDimension>
void writeToFile(
    const std::string &filename,
    const Eigen::Matrix<double, VoigtDimension, VoigtDimension> &data,
    const std::string name, const double granularity, const double density,
    const double radius, const double actual_density) {

  std::ofstream file;
  file.open(filename.c_str(), std::ios_base::app);
  // Set the precision
  file << std::setprecision(9);

  file << name;
  file << ",";
  file << radius;
  file << "," << density;
  file << "," << actual_density;
  file << "," << granularity;

  // Loop over matrix and write output to file
  for (int i = 0; i < VoigtDimension; ++i) {
    for (int j = 0; j < VoigtDimension; j++) {
      file << "," << data(i, j);
    }
  }
  file << std::endl;
  // File closes automatically at end of scope!
}
